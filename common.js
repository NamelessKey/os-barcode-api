exports.log = function(msg) {
    console.log(msg);
}
exports.logInfo =  function(msg) {
    console.info(msg);
}

exports.logError = function(msg) {
    console.error(msg);
}

exports.logWarning = function(msg) {
    console.warn(msg);
}