var express = require('express');
var path = require('path');
var usersRouter = require('./routes/users');
var productsRouter = require('./routes/products');
var stockRouter = require('./routes/stocks');
var res404 = require('./model/res404');

var app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// import route
app.use('/users', usersRouter);
app.use('/products', productsRouter);
app.use('/stocks', stockRouter);

app.get('*', function(req, res) {  
    res.status(404);
    res.send(res404.res);
});

const serverPort = 3000;
app.listen(3000, "0.0.0.0", function () {
    console.log(`Example app listening on port ${serverPort}`);
});