var express = require('express');
var router = express.Router();
var config = require('../config');
var res500 = require('../model/res500');
var log = require('../common');
const Odoo = require('odoo-xmlrpc');

var odoo = new Odoo({
  url: config.host, // example 'http://192.168.99.100'
  port: config.port,
  db: config.db,
  username: config.username,
  password: config.password
});

const responseFiled = ['action_id', 'active', 'login', 'login_date', 'partner_id', 'user_email', 'company_id', 'password', 'partner_id', 'signature', 'menu_id'];

router.use(function (req, res, next) {
  // res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
  // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

router.post('/login', function (req, res, next) {
  log.logInfo('req => ' + JSON.stringify(req.body));
  /*authentication using odoo-odoo*/
  const auth = new Odoo({
    url: config.host, // example 'http://192.168.99.100'
    port: config.port,
    db: config.db,
    username: req.body.username,
    password: req.body.password
  });
  auth.connect(function (err) {
    if (err) {
      log.logErr(err);
      return res.send(err);
    }
    log.logInfo('authentication. success.');
    res.send({ 'auth': true });
  });
});

router.get('/user', function (req, res, next) {

  /*authentication using odoo-odoo*/
  odoo.connect(function (err) {
    if (err) {
      log.logErr(err);
      return res.send(err);
    }
    // console.log('Connected to Odoo server.');

    /*params and filter*/
    var inParams = [];
    inParams.push([['active', '=', true]]);

    /* response field */
    inParams.push(responseFiled);

    var params = [];
    params.push(inParams);

    /*execute*/
    odoo.execute_kw('res.users', 'search_read', params, function (err, value) {
      if (err) {
        log.logErr(err);
        return res.send(err);
      }
      res.send(value);
    });
  });
});

router.put('/user', function (req, res, next) {

  /*authentication using odoo-odoo*/
  odoo.connect(function (err) {
    if (err) {
      log.logErr(err);
      return res.send(err);
    }
    // console.log('Connected to Odoo server.');

    /*params and filter*/
    var inParams = [];
    inParams.push([req.body.id]);

    /* update field */
    inParams.push({ 'password': '' + req.body.password + '' });

    var params = [];
    params.push(inParams);

    /*execute*/
    odoo.execute_kw('res.users', 'write', params, function (err, value) {
      if (err) {
        log.logErr(err);
        return res.send(err);
      }
      res.send(value);
    });
  });
});

module.exports = router;
