var express = require('express');
var router = express.Router();
var config = require('../config');
var log = require('../common');
var dateFormat = require('dateformat');

const Odoo = require('odoo-xmlrpc');

var odoo = new Odoo({
    url: config.host, // example 'http://192.168.99.100'
    port: config.port,
    db: config.db,
    username: config.username,
    password: config.password
});

const responseFiled = ['default_code', 'active', 'company_id', 'name', 'display_name', 'property_stock_production', 'cate_name', 'company_id', 'price', 'qty_available']
router.use(function (req, res, next) {
    // res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
    // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

router.get('/all', function (req, res, next) {
    /*authentication using odoo-odoo*/
    odoo.connect(function (err) {
        if (err) {
            log.logErr(err);
            return res.send(err);
        }
        /*params and filter*/
        var inParams = [];
        var params = [];
        params.push(inParams);

        /*execute*/
        odoo.execute_kw('stock.inventory', 'search_read', params, function (err, value) {
            if (err) {
                log.logErr(err);
                return res.send(err);
            }
            res.send(value);
        });
    });
});

router.get('/picking', function (req, res, next) {
    /*authentication using odoo-odoo*/
    odoo.connect(function (err) {
        if (err) {
            log.logErr(err);
            return res.send(err);
        }
        // log.logInfo('Connected to Odoo server.');

        /*params and filter*/
        var inParams = [];
        // inParams.push([
        //     ['active', '=', true],
        //     ['price', '!=', 0]
        //     // ['password','=','' + req.body.password + '']
        // ]);

        // inParams.push(responseFiled); //fields
        // inParams.push(['active', 'login']);

        var params = [];
        params.push(inParams);

        /*execute*/
        odoo.execute_kw('stock.picking', 'search_read', params, function (err, value) {
            if (err) {
                log.logErr(err);
                return res.send(err);
            }
            res.send(value);
        });
    });
});

router.post('/createPo', function (req, res, next) {
    log.logInfo(JSON.stringify(req.body));
    /*authentication using odoo-odoo*/
    odoo.connect(function (err) {
        if (err) {
            log.logErr(err);
            return res.send({
                resStatus: false,
                resMessage: 'odoo connect fail',
                data: { err }
            });
        }
        // log.logInfo('Connected to Odoo server.');
        log.logInfo(dateFormat(Date().queue, "dd-mm-yyyy HH:MM:ss"));
        /*params and filter*/
        var inParams = [];
        inParams.push({
            'name': 'Adjustments ' + dateFormat(Date().queue, "dd-mm-yyyy HH:MM:ss"),
            'filter': 'partial',
        })

        // inParams.push(responseFiled); //fields
        // inParams.push(['active', 'login']);
        log.logInfo("inventory ", inParams)
        var params = [];
        params.push(inParams);

        /*execute*/
        odoo.execute_kw('stock.inventory', 'create', params, function (err, value) {
            if (err) {
                log.logErr(err);
                return res.send({
                    resStatus: false,
                    resMessage: 'create stock inventory error',
                    data: { err }
                });
            }
            log.logInfo('Inventory Id: ' + value);
            var inParams = [];
            var inventory_id = value;
            inParams.push([inventory_id])
            var inventory_params = [];
            inventory_params.push(inParams);
            odoo.execute_kw('stock.inventory', 'read', inventory_params, function (err2, inventory_obj) {
                if (err2) {
                    log.logErr(err2);
                    return res.send({
                        resStatus: false,
                        resMessage: 'read stock inventory error',
                        data: { err2 }
                    });
                }
                log.logInfo('Inventory : ' + JSON.stringify(inventory_obj));
                // อ่านไฟล์ Json เพื่อดึงข้อมูลที่มีอยู่มาใส่ list
                let countAcount = 0;
                for (let i = countAcount; i < req.body.length; i++) {
                    log.logInfo(i);
                    // เขียนข้อมูลที่อ่านจากไฟล์ Json ลงตัวแปร inParams เพื่อ create 'stock.inventory.line'
                    try {
                        var inParams = [];
                        inParams.push({
                            'inventory_id': inventory_id,
                            'product_id': req.body[i].productID,
                            'product_uom_id': req.body[i].uomID,
                            'product_qty': req.body[i].productQty,
                            'location_id': inventory_obj[0].location_id[0],
                        })
                        var params = [];
                        params.push(inParams);
                        log.logInfo("line : " + JSON.stringify(params))
                        odoo.execute_kw('stock.inventory.line', 'create', params, function (err4, line_id) {
                            if (err4) {
                                log.logError("Create[" + i.toString() + "] 'stock.inventory.line' => " + JSON.stringify(err4));
                                return;
                            }
                        });
                        countAcount++;
                    } catch (err) {
                        log.logError(err);
                        return;
                    } finally {
                    }
                }
                
                odoo.execute_kw('stock.inventory', 'action_start', inventory_params, function (err3, action_start) {
                    if (err3) {
                        log.logError(err3);
                        res.send({
                            resStatus: false,
                            resMessage: 'action_start stock inventory error',
                            data: { err3 }
                        });
                        return;
                    }
                    log.logInfo("Start Inventory => " + action_start);

                    res.send({
                        resStatus: true,
                        resMessage: 'create stock inventory success',
                        data: {
                            inventoryID: inventory_id,
                            inventoryName: inventory_obj[0].name,
                        }
                    });

                    // var inParams = [];
                    // inParams.push([parseInt(inventory_id)])
                    // var inventory_params = [];
                    // inventory_params.push(inParams);
                    // odoo.execute_kw('stock.inventory', 'action_done', inventory_params, function (err3, action_done) {
                    //     if (err3) { 
                    //         log.logError(err3); 
                    //         return;
                    //     }
                    //     log.logInfo("Validate Inventory => " + JSON.stringify(action_done));

                    //     res.send({
                    //         resStatus: true,
                    //         resMessage: 'create stock inventory success',
                    //         data: {
                    //             inventoryID: inventory_id,
                    //             inventoryName: inventory_obj[0].name,
                    //         }
                    //     });
                    // });
                });
            });
        });
    });
});

module.exports = router;
