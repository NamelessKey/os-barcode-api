var express = require('express');
var router = express.Router();
var config = require('../config');
var log = require('../common');
const Odoo = require('odoo-xmlrpc');

var odoo = new Odoo({
  url: config.host, // example 'http://192.168.99.100'
  port: config.port,
  db: config.db,
  username: config.username,
  password: config.password
});

const responseFiled = ['default_code', 'active', 'company_id', 'name', 'display_name', 'property_stock_production', 'cate_name', 'company_id', 'price', 'qty_available', 'uom_id', 'location_id']
router.use(function (req, res, next) {
  // res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
  // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

router.get('/all', function (req, res, next) {
  /*authentication using odoo-odoo*/
  odoo.connect(function (err) {
    if (err) {
      log.logErr(err);
      return res.send(err);
    }
    // console.log('Connected to Odoo server.');

    /*params and filter*/
    var inParams = [];
    inParams.push([
      ['active', '=', true],
      ['price', '>', 0],
      ['qty_available', '>', 0]
      // ['password','=','' + req.body.password + '']
    ]);

    inParams.push(responseFiled); //fields
    // inParams.push(['active', 'login']);

    var params = [];
    params.push(inParams);

    /*execute*/
    odoo.execute_kw('product.product', 'search_read', params, function (err, value) {
      if (err) {
        log.logErr(err);
        return res.send(err);
      }
      res.send(value);
    });
  });
});

router.get('/scan', function (req, res, next) {
  // console.log('req => ' + JSON.stringify(req.body));
  log.logInfo('param => ' + req.query.productCode);
  /*authentication using odoo-odoo*/
  odoo.connect(function (err) {
    if (err) {
      return res.send(err);
    }
    // console.log('Connected to Odoo server.');

    /*params and filter*/
    var inParams = [];
    inParams.push([
      ['active', '=', true],
      ['default_code', '=', '' + req.query.productCode + '']
      // ['password','=','' + req.body.password + '']
    ]);

    inParams.push(responseFiled); //fields
    // inParams.push(['active', 'login']);

    var params = [];
    params.push(inParams);

    /*execute*/
    odoo.execute_kw('product.product', 'search_read', params, function (err, value) {
      if (err) {
        log.logErr(err);
        return res.send(err);
      }
      log.logInfo(JSON.stringify(value));
      if (Number(value[0].qty_available) <= 0) {
        res.send({
          resStatus: false,
          resMessage: 'product qty_available <= 0'
        });
      } else {
        res.send({
          resStatus: true,
          resMessage: '',
          data: { value }
        }); 
      }
    });
  });
});

module.exports = router;
